export class d {
    animEl: NodeList;

    constructor() {
        this.collectElements();
        this.createObserver();
    }

    collectElements() {
        this.animEl =
            document.querySelectorAll('[data-anim]');
    }


    createObserver() {
        if (!this.animEl.length) return

        const observerOptions = {
            rootMargin: "0px",
            threshold: [0, 0.5, 1],
        };

        let observer = new IntersectionObserver(observerCallback.bind(this), observerOptions);

        function observerCallback(entries, observer) {
            entries.forEach((entry) => {
                const leaveClass = entry.target.dataset.animLeave
                const enterClass = entry.target.dataset.animEnter
                if (!leaveClass || !enterClass) return;
                //console.info(entry.intersectionRatio)
                if (entry.isIntersecting) {
                    //console.info("Entering");
                    // entry.target.classList.remove(leaveClass);
                    // entry.target.classList.add(enterClass);
                }

                if (entry.intersectionRatio < 0.3) {
                    entry.target.classList.remove(enterClass);
                    entry.target.classList.add(leaveClass);
                } else {
                    entry.target.classList.remove(leaveClass);
                    entry.target.classList.add(enterClass);

                }
            })
        }

        this.animEl.forEach(el => {
            observer.observe(el);
        })

    }
}