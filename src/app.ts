import throttle from "lodash.throttle";

export class d {
    infoButton
    wrapper
    scrollIndicator
    scrollDownButtons
    backToTopButton
    echtzeit


    constructor(ezInstance) {
        this.echtzeit = ezInstance;
        this.recalcVH();

        this.infoButton = document.querySelector('#infoButton');
        this.wrapper = document.querySelector('#wrapper');
        this.scrollIndicator = document.querySelector('#scrollIndicator');
        this.scrollDownButtons = document.querySelectorAll('.scrollDownButton');
        this.backToTopButton = document.querySelector('#backToTopButton');

        // Infobutton setup (hides wrapper to let user interact with 3d)
        // Shows ar button.
        const handleInfoButton = function (ev) {
            if (this.wrapper.classList.contains("hide")) {
                this.wrapper.classList.remove("hide");
                this.backToTopButton.classList.remove("hide");
                ezInstance.arButton('hide');
            } else {
                this.wrapper.classList.add("hide");
                this.backToTopButton.classList.add("hide");
                ezInstance.shot("35a9e04b908541f1ab9b8221db93b98f");
                ezInstance.arButton('show');
            }

        }



        if (this.backToTopButton) {
            this.backToTopButton.addEventListener('click', this.disableScrollTrigger.bind(this))
        }

        if (this.infoButton && this.wrapper) {
            this.infoButton.addEventListener('click', handleInfoButton.bind(this))
        }

        // Scroll Indicator
        const handleScroll = function (ev) {
            var winScroll = ev.target.scrollTop;
            var height = ev.target.scrollHeight - ev.target.clientHeight;
            var scrolled = (winScroll / height) * 100;
            this.scrollIndicator.style.width = scrolled + "%";

            // hide backToTobButton in hero section
            if (!this.backToTopButton) return;
            if (scrolled < 10) {
                this.backToTopButton.classList.add("inHero");
            } else {
                this.backToTopButton.classList.remove("inHero");
            }
        }

        if (this.wrapper && this.scrollIndicator) {
            this.wrapper.addEventListener('scroll', throttle(handleScroll.bind(this), 100))
        }


        // resizing
        window.addEventListener('resize', () => {
            // We execute the same script as before
            this.recalcVH();

        });



    }
    // Resizing see https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
    recalcVH(): void {
        let vh = window.innerHeight * 0.01;
        //document.documentElement.style.setProperty('--vh', `${vh}px`);
        console.info("1 vh equals ", vh, 'px');
        //const heroHeadline = document.querySelector('#heroHeadline');
        //heroHeadline.innerHTML=vh + 'px!?wk'
    }

    // Temporarily disable scroll trigger when going back to top
    disableScrollTrigger = function () {
        this.echtzeit.shot("8c1b4c4cc4d74fb3ac82b852c80a4fd2");
        this.echtzeit.onEnterAllow = false;
        setTimeout(function () {
            this.echtzeit.onEnterAllow = true;
        }.bind(this), 2000);
    }

}