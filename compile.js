const sass = require('sass');
const pug = require('pug');
const fs = require('fs');

const compressed = sass.compile("./src/app.scss", { style: "compressed" });

fs.writeFileSync('./dist/__tmp.css', compressed.css);


const compiledPug = pug.renderFile('./src/template.pug');
fs.writeFileSync('./dist/index.html', compiledPug);
